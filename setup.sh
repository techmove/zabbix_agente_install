#!/bin/bash
#=====================================================================+
#		SCRIPT DE INSTALACAO AGENTE ZABBIX		
#=====================================================================+
#CRIADO POR	: LUIZ PAULO PIMENTA
#DATA		: 17/12/2020
#=====================================================================+

source ./includes.sh


echo -e $WHITE =============================================================== $FECHA
echo -e $GREEN   INICIANDO A INSTALAÇÃO DO AGENTE ZABBIX COM O PROCESSO $$ $FECHA
echo -e $WHITE =============================================================== $FECHA
echo -e $GREEN SCRIPT CRIADO POR LUIZ PAULO PIMENTA $FECHA

echo .
echo .

# SALVA DIRETORIO ATUAL DA INSTALACAO
dir=$(pwd)


echo -e $YELLOW VERIFICANDO VERSAO DO SISTEMA OPERACIONAL $FECHA
soVersion=$(cat /etc/os-release | grep -i codename | cut -d"=" -f2 | tail -n 1)


#VERIFICA SE RETORNOU VAZIO
if [ -z $soVersion ] ; then

	soVersion=$(cat /etc/os-release | grep -i version= | cut -d"=" -f2 | awk '{print $2}' | sed 's/(//g' | sed 's/)"//g' | tail -n 1 ) 
fi



echo -e $YELLOW A VERSÃO DO SISTEMA É $soVersion


case $soVersion in

	"stretch")

		echo -e $YELLOW EFETUANDO DOWNLOAD DO REPOSITORIO PARA DEBIAN STRETCH $FECHA
		wget -c https://repo.zabbix.com/zabbix/4.4/debian/pool/main/z/zabbix-release/zabbix-release_4.4-1%2Bstretch_all.deb
	;;

	"jessie")

		echo -e $YELLOW EFETUANDO DOWNLOAD DO REPOSITORIO PARA DEBIAN JESSIE $FECHA
		wget -c https://repo.zabbix.com/zabbix/4.4/debian/pool/main/z/zabbix-release/zabbix-release_4.4-1%2Bjessie_all.deb
	;;

	"bionic")

		echo -e $YELLOW EFETUANDO DOWNLOAD DO REPOSITORIO PARA UBUNTU BIONIC $FECHA
		wget -c https://repo.zabbix.com/zabbix/4.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.4-1%2Bbionic_all.deb 

	;;
	
	"buster")

		echo -e $YELLOW EFETUANDO DOWNLOAD DO REPOSITORIO PARA DEBIAN BUSTER$FECHA
		wget -c https://repo.zabbix.com/zabbix/4.4/debian/pool/main/z/zabbix-release/zabbix-release_4.4-1%2Bbuster_all.deb 

	;;

	"bookworm")

		echo -e $YELLOW EFETUANDO DOWNLOAD DO REPOSITORIO PARA DEBIAN BOOKWORM$FECHA
		wget https://repo.zabbix.com/zabbix/6.0/debian/pool/main/z/zabbix-release/zabbix-release_6.0-5+debian12_all.deb

	;;

	
	"bullseye")

		echo -e $YELLOW EFETUANDO DOWNLOAD DO REPOSITORIO PARA DEBIAN BULLSEYE $FECHA
		wget https://repo.zabbix.com/zabbix/6.0/debian/pool/main/z/zabbix-release/zabbix-release_6.0-4+debian11_all.deb

	;;
	
	
	*)
		echo -e $RED !!! ATENÇÃO !!!! VERSÃO NÃO SUPORTADA $FECHA
		exit 0
	;;

esac 

echo .
echo .

sleep 3

echo -e $YELLOW EFETUANDO CONFIGURAÇÃO DO REPOSITORIO $FECHA
dpkg -i zabbix*.deb

clear
echo -e $YELLOW ATUALIZANDO REPOSITORIOS $FECHA
apt update

sleep 3

#VALIDA VERSAO DO AGENTE
agenteVersion=$(aptitude search zabbix-agent2)

clear
if [ -z $agenteVersion ] ; then

	echo -e $YELLOW INSTALANDO AGENTE ZABBIX $FECHA
	apt install zabbix-agent

	echo -e $YELLOW EFETUANDO CONFIGURACAO DO AGENTE $FECHA
	cat conf/zabbix_agentd.conf > /etc/zabbix/zabbix_agentd.conf


	echo -e $YELLOW EFETUANDO AJUSTE NO ARQUIVO CONFIGURAÇÃO DO AGENTE $FECHA
	host=$(hostname)

	sed -i "s/hostname/$host/g" /etc/zabbix/zabbix_agentd.conf


	if [ -z $1 ] ; then

		echo -e $YELLOW INFORME O IP DO SERVIDOR ZABBIX$FECHA
		read ipServidor
	
	else

		ipServidor=$1

	fi	

	sed -i "s/ip_servidor/$ipServidor/g" /etc/zabbix/zabbix_agentd.conf


	echo -e $YELLOW EFETUANDO DOWNLOAD DO REPOSITORIO DE CONFIGURACOES$FECHA
	cd /etc/zabbix
	git clone git@bitbucket.org:techmove/zabbix_scripts.git


	cd $dir


	echo -e $YELLOW AJUSTANDO PERMISSOES USUARIO ZABBIX$FECHA
	sed -i '/zabbix/d' /etc/passwd
	echo zabbix:x:0:0::/nonexistent:/bin/false >> /etc/passwd


	sleep 3
	clear
	echo -e $YELLOW INICIALIZANDO SERVICO DO AGENTE$FECHA
	service zabbix-agent start
	systemctl enable zabbix-agent
	service zabbix-agent restart


	echo -e $YELLOW VALIDANDO SE O SERVICO ESTA ATIVO$FECHA
	valida=$(service zabbix-agent status | grep -i active | awk '{print $3}' | sed 's/(//g' | sed 's/)//g')


else

	echo -e $YELLOW INSTALANDO AGENTE ZABBIX $FECHA
	apt install zabbix-agent2

	echo -e $YELLOW EFETUANDO CONFIGURACAO DO AGENTE $FECHA
	cat conf/zabbix_agent2.conf > /etc/zabbix/zabbix_agent2.conf


	echo -e $YELLOW EFETUANDO AJUSTE NO ARQUIVO CONFIGURAÇÃO DO AGENTE $FECHA
	host=$(hostname)

	sed -i "s/hostname/$host/g" /etc/zabbix/zabbix_agent2.conf


	if [ -z $1 ] ; then

		echo -e $YELLOW INFORME O IP DO SERVIDOR ZABBIX$FECHA
		read ipServidor
	
	else

		ipServidor=$1

	fi	

	sed -i "s/ip_servidor/$ipServidor/g" /etc/zabbix/zabbix_agent2.conf


	echo -e $YELLOW EFETUANDO DOWNLOAD DO REPOSITORIO DE CONFIGURACOES$FECHA
	cd /etc/zabbix
	git clone git@bitbucket.org:techmove/zabbix_scripts.git


	cd $dir


	echo -e $YELLOW AJUSTANDO PERMISSOES USUARIO ZABBIX$FECHA
	sed -i '/zabbix/d' /etc/passwd
	echo zabbix:x:0:0::/nonexistent:/bin/false >> /etc/passwd


	sleep 3
	clear
	echo -e $YELLOW INICIALIZANDO SERVICO DO AGENTE$FECHA
	service zabbix-agent2 start
	systemctl enable zabbix-agent2
	service zabbix-agent2 restart


	echo -e $YELLOW VALIDANDO SE O SERVICO ESTA ATIVO$FECHA
	valida=$(service zabbix-agent2 status | grep -i active | awk '{print $3}' | sed 's/(//g' | sed 's/)//g')


fi	


sleep 2
clear
if [ $valida == "running" ] ; then

echo -e $GREEN AGENTE CONFIGURADO COM SUCESSO $FECHA
echo -e $GREEN VA ATE O PAINEL DO SERVIDOR ZABBIX E CRIE UM HOST COM O SEGUINTE NOME $host $FECHA

else

echo -e $RED HOUVE UM PROBLEMA DURANTE A INSTALACAO E O AGENTE ESTA PARADO. NECESSARIO INTERVENCAO MANUAL $FECHA

fi

